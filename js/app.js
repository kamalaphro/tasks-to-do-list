var LearningAngular = angular.module('LearningAngular', [
    'ui.router',
]);

LearningAngular.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/page-not-found');

    $stateProvider
        .state('view1', {
            url: '/prashant',
            templateUrl: 'views/view1.html',
        })
        .state('view2', {
            url: '/view2',
            templateUrl: 'views/view2.html',
        })
        .state('view3', {
            url: '/view3',
            templateUrl: 'views/view3.html',
        })
        .state('pagenotfound', {
            url: '/page-not-found',
            templateUrl: 'views/page-not-found.html',
        })
    ;
}]);

LearningAngular.controller('MainController', ['$scope', '$timeout', '$http', function($scope, $timeout, $http) {
    $scope.greeting = 'prashant';
    $scope.tasks = [];

    $scope.addTask = function() {
        $scope.tasks.push({
            name: $scope.task,
            isCompleted: false
        });
        $scope.task = '';
    };

    $scope.markTaskComplete = function (task) {
        task.isCompleted = true;
    };

    $scope.plans = [];
    $http({
        url: 'http://54.169.223.200/api/plans',
        method: 'get',
        params: {

        }
    })
    .then(function(response) {
        console.log(response);
        if (response.data.status == 'success') {
            $scope.plans = response.data.data;
        }
    }, function(error) {
        console.log(error);
    })
}]);